package be.kdg.scrum;

import java.util.HashMap;
import java.util.Map;

public class Backlog
{
    private final Map<Integer, UserStory> stories;

    public Backlog() {
        this.stories = new HashMap<>();
    }

    public void addStory(UserStory story)  {
        if(!stories.containsKey(story.getId()))
            stories.put(story.getId(), story);
    }

    public void removeStory(int storyId){
        stories.remove(storyId);
    }

    public UserStory getStory(int storyId){
        return stories.get(storyId);
    }

    public int countStories(){
        return stories.size();
    }
}

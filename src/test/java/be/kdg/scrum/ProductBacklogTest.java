package be.kdg.scrum;

import be.kdg.scrum.Backlog;
import be.kdg.scrum.UserStory;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Author: Jan de Rijke
 */
public class ProductBacklogTest {
	private Backlog bl = new Backlog();
	private List<UserStory> stories = new ArrayList<>();
	private Exception lastException = null;
	private UserStory currentUserStory = null;

	@Given("User Stories")
	public void userStories(DataTable dataTable) {
		for (Map<String, String> map : dataTable.asMaps()) {
			stories.add(new UserStory(
				Integer.parseInt(map.get("storyId")),
				map.get("text"),
				map.get("notes"),
				Integer.parseInt(map.get("priority"))));
		}
	}

	@Given("The Backlog has {int} user stories")
	public void theBacklogHasUserStories(int arg0) {
		for (int i = 0; i < arg0; i++) {
			bl.addStory(stories.get(i));
		}
	}

	@When("I add {int} new user stories")
	public void iAddNewUserStories(int arg0) {
		int aantal = bl.countStories();
		for (int i = aantal; i < aantal + arg0; i++) {
			bl.addStory(stories.get(i));
		}
	}

	@Then("there are {int} user stories in the backlog")
	public void thereAreUserStoriesInTheBacklog(int arg0) {
		assertEquals(bl.countStories(), arg0);
	}


	@When("I add user story {int} with priority {int}")
	public void iAddUserStoryWithPriority(int arg0, int arg1) {
		bl.addStory(new UserStory(arg0, "geen omschrijving", "geen toevoeging", arg1));
	}

	@And("user story {int} has priority {int}")
	public void userStoryHasPriority(int arg0, int arg1) {
		assertEquals(arg1, bl.getStory(arg0).getPriority());
	}

	@When("I ask to get user story with id {int}")
	public void iAskToGetUserStoryWithId(int arg0) {
		currentUserStory = bl.getStory(arg0);
	}

	@Then("the programs returns the user story with id {int}")
	public void theProgramsReturnsTheUserStoryWithId(int arg0) {
		assertEquals(arg0, currentUserStory.getId());
	}


}

